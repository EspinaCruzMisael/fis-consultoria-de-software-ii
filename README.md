# Nombre: Nearsoft
####  Logotipo: 
![Una imagen](imagenes/una-imagen.jpg)

#### Sitio web:
https://nearsoft.com/
#### Ubicación: 
 https://goo.gl/maps/hh6SPqsaABSTj4rV6
#### Acerca de: 
Somos una empresa que se dedica al desarrollo de Software en conjunto con empresas de Estados Unidos, con una cultura basada en la individualidad, libertad y responsabilidad de sus integrantes.
#### Servicios: 
Desarrollo, pruebas, investigación y ejecución de UX / UI de software.
#### Presencia:
* https://www.facebook.com/Nearsoft/
* https://twitter.com/Nearsoft
* https://www.linkedin.com/company/nearsoft/
#### Ofertas Laborales:
https://nearsoft.com/join-us/
#### Blog de Ingeniería: 
https://nearsoft.com/blog/
#### Tecnologías: 
* Java
* Ruby
* Reaccionar
* Pitón
* Nodo
* DevOps
* Completa pila
* WordPress

